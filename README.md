# GrowthViz

https://petrakaferle.shinyapps.io/GrowthViz/

A spectrometer measures optical density of cells in liquid media. Normally, it reads 96 samples every 2 minutes. The information we obtain through this experiment is how fast cells grow in specific conditions. 

The app log-transforms raw data and enables comparing up to 12 samples of interest. It starts with overview of all 96 samples to guide the user in his data exploration. Individual samples can be visualized independently or in comparison. Colour palette is chosen to have light/dark pairs of individual colours, because sample comparison is often pair-wise. Since run-time of experiment does not always correspond to time-frame of interest, a user can chose up to which time point he would like to visualize the data. The default generic legend is modifiable and it can be downloaded or uploaded. 

![Capt_GrowthViz](/uploads/1ba7d3eb0dd6448b6cc6937d091874e9/Capt_GrowthViz.PNG)

